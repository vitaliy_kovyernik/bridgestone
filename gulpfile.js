var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    jade = require('gulp-jade'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    spritesmith = require('gulp.spritesmith'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    buffer = require("vinyl-buffer"), //для спрайтов, не понятная тема
  //less = require("gulp-less"),

    reload = browserSync.reload;



var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/',
        js: 'build/js/',
        btstrp:'build/css/bootstrap/',
        css: 'build/css/',
        sprites: 'build/img/sprites/',
        img: 'build/img/',
        imgContent: 'build/img/upload/',
        fonts: 'build/fonts/',
        toCopy: 'build/icons'
    },
    src: { //Пути откуда брать исходники
        html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        jade: 'src/**/*.jade',
        js: 'src/js/main.js',//В стилях и скриптах нам понадобятся только main файлы,
        btstrp:'bower_components/bootstrap-sass/assets/stylesheets/_bootstrap.scss',
        style: 'src/scss/main.scss',
        img: 'src/img/**/*.*',
        imgContent: 'src/imgContent/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        sprites: 'src/img/sprites/**/*.*',
        fonts: 'src/fonts/*.*',
        toCopy: 'src/icons/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/**/*.html',
        jade: 'src/**/*.jade',
        js: 'src/js/**/*.js',
        style: 'src/scss/*.scss',
        img: 'src/img/**/*.*',
        sprites: 'src/sprites/**/*.*',
        fonts: 'src/fonts/*.*',
        toCopy: 'src/icons/**/*.*',
        less:'bower_components/bootstrap/less/**/*.less',
    },
    clean: './build'
};



var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost/',
    port: 9000,
    logPrefix: "Frontend_Vetal"
};



 gulp.task('hello', function() {
  console.log('Hello Vitaly');
});




//////////////////JADE///////////////////
gulp.task('jade:b',function () {
    gulp.src(path.src.jade)
        .pipe(sourcemaps.init())
        .pipe(jade({pretty: true}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});
gulp.task('jade:w', function () {
  gulp.watch(path.src.jade, ['jade']);
});


///////////////////JS/////////////////////

gulp.task('js:b',function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        // .pipe(sourcemaps.init())
        // .pipe(uglify())
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('js:w', function () {
  gulp.watch(path.src.js, ['js']);
});




//////////////////SASS//////////////////
gulp.task('scss:b',function () {
    return gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass({
            //outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});
///////////////////bootstrap////////////
// gulp.task('btstrp:b',function(){
//   return gulp.src(path.src.btstrp)
//       .pipe(sourcemaps.init())
//         .pipe(sass().on('error', sass.logError))
//         .pipe(sourcemaps.write())
//         .pipe(gulp.dest(path.build.btstrp))
//         .pipe(reload({stream: true}));
// })


gulp.task('scss:w', function () {
  gulp.watch(path.src.style, ['scss']);
});



//////////////////IMAGES////////////////
gulp.task('image:b', function () {
    gulp.src(path.src.img) //Выберем наши картинки
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({stream: true}));
});

gulp.task('imageContent:b', function () {
    gulp.src(path.src.imgContent) //Выберем наши картинки
        .pipe(imagemin({ //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.imgContent)) //И бросим в build
        .pipe(reload({stream: true}));
});

gulp.task('sprites:b', function () {
  var spriteData = gulp.src(path.src.sprites).pipe(spritesmith({
    imgName: '../img/sprites/sprite.png',
    cssFormat: 'css',
    cssName: 'sprite.scss',
    imagePath: '../img/sprites'
  //  algorithm: 'binary-tree',
  //  retinaSrcFilter: [path.src.sprites+'/*@2x.png'],
  //  retinaImgName: 'sprite@2x.png',
  }));

    //return imgStream = 
    spriteData.img
    // DEV: We must buffer our stream into a Buffer for `imagemin`
       // .pipe(buffer())
        //.pipe(imagemin())
        .pipe(gulp.dest(path.build.sprites));

    spriteData.css.pipe(gulp.dest(path.build.css));


});











////////////////////FONTS//////////////////
gulp.task('fonts:b', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});





////////////////////BUILD ALL//////////////

gulp.task('build', [
    //'html:b',
    'jade:b',
    'js:b',
    'scss:b',
    'fonts:b',
    'image:b',
    'imageContent:b',
    'sprites:b'
    // 'btstrp:b'
]);





//////////////////////WATCH ALL///////////
gulp.task('watch', function(){
    watch([path.watch.jade], function(event, cb) {
        gulp.start('jade:b');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('scss:b');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:b');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:b');
    });
    watch([path.watch.imgContent], function(event, cb) {
        gulp.start('imageContent:b');
    });
    watch([path.watch.sprites], function(event, cb) {
        gulp.start('sprites:b');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:b');
    });
    watch([path.watch.toCopy], function(event, cb) {
        gulp.start('icons:b');
    });
    //  watch([path.watch.less], function(event, cb) {
    //     gulp.start('btstrp:b');
    // });
});

gulp.task('default', ['build', 'webserver', 'watch']);



//////////////////////SERVER/////////////////
gulp.task('webserver', function () {
    browserSync(config);
});



///////////clean build folder////////////////
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});




////////////////////soc icons//////////////////
gulp.task('icons:b', function() {
    gulp.src(path.src.toCopy)
        .pipe(gulp.dest(path.build.toCopy))
});
