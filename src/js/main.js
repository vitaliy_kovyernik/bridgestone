//= flatpickr.min.js
$(document).ready(function(){
	
	var $ws=$('.workspace-content');
	if($ws.length>0){

		var $labels=$ws.find('label')

		$labels.find('input[type=radio]').change(function(){
			console.log('in change', this)
			var _this=this;
			$labels.each(function(){
				$(this).removeClass('active');
			});
			
			if(this.checked)
				$(this).parent().addClass('active');
			
		})
	}









	//////////////////////////////////
	/////////////////////CALENDAR//////////////////////////
		/* Russian locals for flatpickr */

		Flatpickr.l10n.weekdays = {
			shorthand: ['Вс','Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
			longhand: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота']
		};

		Flatpickr.l10n.months = {
			shorthand: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
			longhand: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
		};
		Flatpickr.l10n.firstDayOfWeek = 1;

		//Загруженность дней
		var dayLoad = {
			hard:
				{
					trainings: 15,
					color: 'red'
				},
			middle:
				{
					trainings: 10,
					color: 'orange'
				},
			light:
				{
					trainings: 1,
					color: '#DAE1E9'
				},
		};
		
		//Инициализация календаря
		var calendar = $ws.find('#calendar');
		calendar.flatpickr({
			// при клике на дату
			onChange: function(dateObj, dateStr, instance) {

				//если клик по дате с предыдущим месяцем
				if(dateObj.getMonth()<curMonth)
					numerLoad(-1);
				//если клик по дате со следующим месяцем
				else if(dateObj.getMonth()>curMonth)
					numerLoad(1);
				//если клик по дате по текущему месяцу
				else 
					numerLoad(0);
			
			},
			
			// при включении календаря
			enable: [
				function(date){
					return date;
				}
			],
			onOpen: function(dateObj, dateStr, instance){
				return loadDays();
				// console.log(dateObj.getDate());
			},
		});


		//Считаем уровень загруженности
		function middleLoadness(trainings){
			if(trainings==dayLoad.light.trainings)
				return dayLoad.light.trainings;
			// if(trainings<=dayLoad.middle.trainings && trainings>dayLoad.light.trainings)
			// 	return dayLoad.middle.trainings;
			// if(trainings<=dayLoad.hard.trainings && trainings>dayLoad.middle.trainings)
			// 	return dayLoad.hard.trainings;
		}
		
		//Устанавливаем дату(день, месяц) в атрибуты дня
		var curMonth = new Date().getMonth(),
			curYear = new Date().getFullYear();
	
		function numerDays(cm,cy){
			// console.log(typeof cm)
			$('.flatpickr-day').each(function(){

				if($(this).hasClass('prevMonthDay'))		
					$(this).attr('id', 'date-' + $(this).text() + "-" + (cm-1)+ "-" +curYear);
				else 
					if($(this).hasClass('nextMonthDay'))
							$(this).attr('id', 'date-' + $(this).text() + "-" + (cm+1)+ "-" +curYear);
					else
						$(this).attr('id', 'date-' + $(this).text() + "-" + cm + "-" +curYear);
				
				 $(this).click(function(e){
				 	if($(this).attr('load')!=1)
				 		return false;
				 	
				 		
				 })
			});
		}
		// Проставляем загрузку на конкретные даты конкретного месяца
		function loadDays_(month){

			// идем по массиву
			for(var i=0; i<dateObj.length; i++){

				if(month == dateObj[i].month && curYear==dateObj[i].year){
					//загружаем день

					var dayToPaint = $('#date-' + dateObj[i].day + "-" + dateObj[i].month + '-' + dateObj[i].year);
					dayToPaint.attr('load', dateObj[i].load);
					//красим день
					switch(middleLoadness(dateObj[i].load)){
						case dayLoad.light.trainings:
							dayToPaint.css('background-color',dayLoad.light.color);
							break;
						case dayLoad.middle.trainings:
							dayToPaint.css('background-color',dayLoad.middle.color);
							break;
						case dayLoad.hard.trainings:
							dayToPaint.css('background-color',dayLoad.hard.color);
							break;
					}
				}
			}
		}
		numerLoad(0);

		//Обновляем календарь при смене месяца 
		$('.flatpickr-calendar').on('click','.flatpickr-next-month svg', function(){
			numerLoad(1);
		});
		$('.flatpickr-calendar').on('click','.flatpickr-prev-month svg', function(){
			numerLoad(-1);
		});

		$('.flatpickr-calendar').on('change','input.cur_year', function(){
			curYear=$(this).val();
			window.setTimeout(function(){
				$('.flatpickr-day:not(.prevMonthDay):first ').click();
				$('#calendar').val('')
			},500);
			window.setTimeout(function(){
				$('.flatpickr-day:not(.prevMonthDay):first ').removeClass('selected');
			},550);
		});

		//для удобства вызова 1 функ вместо 2х
		function numerLoad(i){
			if(i>0){
				numerDays(++curMonth);
				loadDays_(curMonth);
			}else if(i<0){
				numerDays(--curMonth);
				loadDays_(curMonth);
			}else{
				numerDays(curMonth);
				loadDays_(curMonth);
			}
		}

})